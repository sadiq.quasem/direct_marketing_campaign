import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB, MultinomialNB
from src.evaluate import evaluate

df_train = pd.read_csv('./data/final/final_normalized_train.csv')
df_test = pd.read_csv('./data/final/final_normalized_test.csv')

df_train_class = pd.DataFrame(df_train['y'])
df_train_features = df_train.loc[:, df_train.columns != 'y']

df_test_class = pd.DataFrame(df_test['y'])
df_test_features = df_test.loc[:, df_test.columns != 'y']

'''
Create array for continuous variables
'''
continuous_features = ['age', 'campaign', 'previous', 'emp.var.rate', 'cons.price.idx', 'cons.conf.idx', 'euribor3m', 'nr.employed']
# continuous_features = ['age', 'duration', 'campaign', 'previous', 'emp.var.rate', 'cons.price.idx', 'cons.conf.idx', 'euribor3m', 'nr.employed']

'''
Create dataframes for continuous and categorical variables for gaussian and multinomial nb
'''
df_train_features_con = df_train_features.loc[:,df_train_features.columns.isin(continuous_features)]
df_train_features_cat =  df_train_features.loc[:,~df_train_features.columns.isin(continuous_features)]

df_test_features_con = df_test_features.loc[:, df_test_features.columns.isin(continuous_features)]
df_test_features_cat =  df_test_features.loc[:,~df_test_features.columns.isin(continuous_features)]

'''
Fit gaussian model on continuous features
'''
gaussian_nb = GaussianNB()
gaussian_nb.fit(df_train_features_con, df_train_class.values.ravel())

gaussian_predicted_prob_train = gaussian_nb.predict_proba(df_train_features_con)
gaussian_predicted_prob_test = gaussian_nb.predict_proba(df_test_features_con)


'''
Fit multinomial model on categorical features
'''
multinomial_nb = MultinomialNB()
multinomial_nb.fit(df_train_features_cat, df_train_class.values.ravel())

multinomial_predicted_prob_train = multinomial_nb.predict_proba(df_train_features_cat)
multinomial_predicted_prob_test = multinomial_nb.predict_proba(df_test_features_cat)

combined_predicted_prob_train = np.vstack([gaussian_predicted_prob_train[:,1],multinomial_predicted_prob_train[:,1]]).T
combined_predicted_prob_test = np.vstack([gaussian_predicted_prob_test[:,1],multinomial_predicted_prob_test[:,1]]).T

gaussian_nb_final = GaussianNB()
gaussian_nb_final.fit(combined_predicted_prob_train, df_train_class.values.ravel())

predicted_train = gaussian_nb_final.predict(combined_predicted_prob_train)
predicted_test = gaussian_nb_final.predict(combined_predicted_prob_test)

predicted_prob_train = gaussian_nb_final.predict_proba(combined_predicted_prob_train)
predicted_prob_test = gaussian_nb_final.predict_proba(combined_predicted_prob_test)


with open('./models/naive_bayes/final_nb_normalized_model.sav', 'wb') as f:
    pickle.dump(gaussian_nb_final, f)

evaluate(df_train_class, predicted_train, predicted_prob_train, df_test_class, predicted_test, predicted_prob_test, 'y')
