import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt
import pickle

from src.evaluate import evaluate

'''
Read preprocessed dataset
'''
df_train = pd.read_csv('./data/final/final_normalized_train.csv')
df_test = pd.read_csv('./data/final/final_normalized_test.csv')

'''
Set features and target columns
'''
df_train_class = pd.DataFrame(df_train['y'])
df_train_features = df_train.loc[:, df_train.columns != 'y']

df_test_class = pd.DataFrame(df_test['y'])
df_test_features = df_test.loc[:, df_test.columns != 'y']

df_pca = PCA(n_components=5)
df_pca.fit(df_train_features)
df_train_features = pd.DataFrame(df_pca.transform(df_train_features))
df_test_features = pd.DataFrame(df_pca.transform(df_test_features))

# '''
# Balanace training subsets using StratifiedKFold
# '''
# skf_model = StratifiedKFold(n_splits=5,shuffle=True)
#
# '''
# Grid search parameters
# '''
# C_array = np.linspace(0.5, 2.2, 7)
# gamma_array = np.linspace(0.01, 0.05, 5)
#
#
# '''
# Grid search manual function
# '''
# iterations = 5
# for i in range(0, iterations):
#     print ("---Iteration: ",t)
#     avg_acc = np.zeros(shape=[len(C_array),len(gamma_array)])
#     std_acc = np.zeros(shape=[len(C_array),len(gamma_array)])
#
#     x = 0
#     for c_value in C_array:
#
#         y = 0
#         for gamma_value in gamma_array:
#             print (c_value, gamma_value)
#
#             temp_accuracy_list = []
#             for train_subset_index, cv_index in skf_model.split(df_train_features,df_train_class):
#                 df_train_features_subset = df_train_features.loc[train_subset_index]
#                 df_train_class_subset = df_train_class.loc[train_subset_index]
#                 df_train_features_cv = df_train_features.loc[cv_index]
#                 df_train_class_cv = df_train_class.loc[cv_index]
#
#                 svm_model = SVC(C = c_value, gamma = gamma_value, kernel = 'rbf', class_weight= 'balanced')
#                 svm_model.fit(df_train_features_subset, df_train_class_subset.values.ravel()``)
#                 score_value = svm_model.score(df_train_features_cv, df_train_class_cv)
#                 temp_accuracy_list.append(score_value)
#
#             avg_acc[x,y] = np.mean(temp_accuracy_list)
#             std_acc[x,y] = np.std(temp_accuracy_list)
#             y += 1
#
#         x += 1
#
#     if t==0:
#         final_avg_acc = avg_acc
#         final_std_acc = std_acc
#     else:
#         final_avg_acc = np.dstack([final_avg_acc, avg_acc])
#         final_std_acc = np.dstack([final_std_acc, std_acc])
#
# final_accuracy_mean_list = np.mean(final_avg_acc, axis=2)
# max_ind = np.unravel_index(np.argmax(final_accuracy_mean_list, axis=None), final_accuracy_mean_list.shape)
#
# chosen_C = C_array[max_ind[0]]
# chosen_gamma = gamma_array[max_ind[1]]
#
# print ("Cross validated C value: ", chosen_C)
# print ("Cross validated gamma value: ",chosen_gamma)
#
# svm_model_final = SVC(C = c_value, gamma = gamma_value, kernel = 'rbf', class_weight='balanced', probability=True)
# svm_model_final = SVC(C = 1.5, gamma = 0.05,kernel = 'rbf', class_weight='balanced',probability=True)

svm_model_final = SVC(C = 1.5, gamma = 0.05, kernel = 'rbf', class_weight='balanced', probability=True)

svm_model_final.fit(df_train_features, df_train_class.values.ravel())

predicted_train = svm_model_final.predict(df_train_features)
predicted_test = svm_model_final.predict(df_test_features)

predicted_prob_train = np.array([])
predicted_prob_test = np.array([])

predicted_prob_train = svm_model_final.predict_proba(df_train_features)
predicted_prob_test  = svm_model_final.predict_proba(df_test_features)

with open('./models/svm/final_svm_normalized_model.sav', 'wb') as f:
    pickle.dump(grid_search_svm, f)

evaluate(df_train_class, predicted_train, predicted_prob_train, df_test_class, predicted_test, predicted_prob_test, 'y')
