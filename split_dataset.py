import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from src.preprocessing import feature_engineering, target

def separate_train_test(df):
    df_class = pd.DataFrame(df['y'])
    df_features = df.loc[:, df.columns != 'y']

    df_features_train, df_features_test, df_class_train,  df_class_test = train_test_split(df_features, df_class)

    df_train = pd.concat([df_features_train, df_class_train], axis=1)
    df_test = pd.concat([df_features_test, df_class_test], axis=1)

    return df_train, df_test

def standardize():
    df = pd.read_csv('./data/bank_data.csv')
    df = df.replace(np.nan, 'NaN', regex=True)
    df = df.drop(['Unnamed: 0'], axis = 1)
    df = df.drop(['duration'], axis = 1)
    df_train, df_test = separate_train_test(df)
    df_train, df_test = feature_engineering(df_train, df_test,"standardize")
    df_train, df_test = target(df_train, df_test)

    df_train.to_csv('./data/final/final_standardized_train.csv', index=False)
    df_test.to_csv('./data/final/final_standardized_test.csv', index=False)

standardize()

def normalize():
    df = pd.read_csv('./data/bank_data.csv')
    df = df.replace(np.nan, 'NaN', regex=True)
    df = df.drop(['Unnamed: 0'], axis = 1)
    df = df.drop(['duration'], axis = 1)
    df_train, df_test = separate_train_test(df)
    df_train, df_test = feature_engineering(df_train, df_test,"normalize")
    df_train, df_test = target(df_train, df_test)

    df_train.to_csv('./data/final/final_normalized_train.csv', index=False)
    df_test.to_csv('./data/final/final_normalized_test.csv', index=False)

normalize()
