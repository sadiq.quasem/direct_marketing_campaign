import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.svm import SVC

'''
Feature engineering method for numerical and categorical columns
'''
def feature_engineering(df_train, df_test, continuous):

    delete = ['pdays', 'default']
    continuous = ['age', 'duration', 'campaign', 'previous', 'emp.var.rate', 'cons.price.idx', 'cons.conf.idx', 'euribor3m', 'nr.employed']
    ordered_categorical = ['education', 'housing', 'loan', 'contact', 'month', 'day_of_week', 'poutcome']
    unordered_categorical = ['job', 'marital']

    with_null = ['job','marital','education','housing','loan']
    filled = ['age', 'duration', 'campaign', 'previous', 'emp.var.rate', 'cons.price.idx', 'cons.conf.idx', 'euribor3m', 'nr.employed', 'contact', 'month', 'day_of_week','poutcome']

    '''
    Delete features not used for classification
    '''
    for i in delete:
        del df_train[i]
        del df_test[i]

    '''
    Standardization or Normalization of numerical columns
    '''
    if continuous == "standardize":
        standardization = StandardScaler()
        standardization.fit(df_train[continuous])
        df_train[continuous] = standardization.transform(df_train[continuous])
        df_test[continuous] = standardization.transform(df_test[continuous])

    elif continuous == "normalize":
        min_max_scaling = MinMaxScaler()
        min_max_scaling.fit(df_train[continuous])
        df_train[continuous] = min_max_scaling.transform(df_train[continuous])
        df_test[continuous] = min_max_scaling.transform(df_test[continuous])

    '''
    Label encoding for ordered categorical columns
    '''
    labels = {'education':{'illiterate':0, 'basic.4y':4, 'basic.6y':6, 'basic.9y':9, 'high.school':11, 'professional.course':13, 'university.degree':14},
                  'housing':{'no':0,'yes':1},
                  'loan':{'no':0,'yes':1},
                  'contact':{'telephone':0,'cellular':1},
                  'month':{'jan':1,'feb':2,'mar':3,'apr':4,'may':5,'jun':6,'jul':7,'aug':8,'sep':9,'oct':10,'nov':11,'dec':12},
                  'day_of_week':{'mon':1,'tue':2,'wed':3,'thu':4,'fri':5,'sat':6,'sun':7},
                  'poutcome':{'nonexistent':0,'failure':1,'success':2}}

    for i in ordered_categorical:
        if i not in with_null:
            df_train = df_train.replace({i:labels[i]})
            df_test = df_test.replace({i:labels[i]})

    '''
    SVM imputation for missing values
    '''
    df_train_impute = df_train.loc[:,df_train.columns.isin(filled)]
    df_test_impute = df_test.loc[:,df_test.columns.isin(filled)]

    for i in with_null:
        train_impute = df_train[i]
        test_impute = df_test[i]

        train_impute_filled = train_impute[train_impute != 'NaN']
        train_impute_null = train_impute[train_impute == 'NaN']
        test_impute_null = test_impute[test_impute == 'NaN']

        df_train_impute_train_features = df_train_impute.loc[train_impute_filled.index]
        df_train_impute_test_features = df_train_impute.loc[train_impute_null.index]
        df_test_impute_test_features = df_test_impute.loc[test_impute_null.index]

        svm_model = SVC(gamma='auto')
        svm_model.fit(df_train_impute_train_features, train_impute_filled)
        df_train.loc[df_train_impute_test_features.index, i] = svm_model.predict(df_train_impute_test_features)
        df_test.loc[df_test_impute_test_features.index, i] = svm_model.predict(df_test_impute_test_features)

    '''
    Label categorical features with null values present
    '''
    for i in ordered_categorical:
        if i in with_null:
            df_train = df_train.replace({i:labels[i]})
            df_test = df_test.replace({i:labels[i]})

    '''
    One hot encoding unordered categorical features
    '''
    for i in unordered_categorical:
        label_encoder = LabelEncoder()
        label_encoder.fit(df_train[i])
        df_train[i] = label_encoder.transform(df_train[i])
        df_test[i] = label_encoder.transform(df_test[i])

    one_hot_encoder = OneHotEncoder(sparse=False)
    one_hot_encoder.fit(df_train[unordered_categorical])
    one_hot_encoded_array_train = one_hot_encoder.transform(df_train[unordered_categorical])
    one_hot_encoded_df_train = pd.DataFrame(one_hot_encoded_array_train, index=df_train.index)
    one_hot_encoded_array_test = one_hot_encoder.transform(df_test[unordered_categorical])
    one_hot_encoded_df_test = pd.DataFrame(one_hot_encoded_array_test, index=df_test.index)

    '''
    Concatenate new columns to the dataset
    '''
    df_train = pd.concat([df_train,one_hot_encoded_df_train], axis=1)
    df_test = pd.concat([df_test,one_hot_encoded_df_test], axis=1)

    '''
    Delete original columns that have be one hot one hot encoded
    '''
    df_train = df_train.drop(unordered_categorical, axis=1)
    df_test = df_test.drop(unordered_categorical, axis=1)

    return df_train, df_test

'''
Label encode target column
'''
def target(df_train, df_test):
    target = 'y'
    labels = {target:{'no':0,'yes':1}}
    df_train = df_train.replace({target:labels[target]})
    df_test = df_test.replace({target:labels[target]})
    return df_train, df_test
