import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import StratifiedKFold
import matplotlib.pyplot as plt
import pickle

from src.evaluate import evaluate

'''
Read preprocessed dataset
'''
df_train = pd.read_csv('./data/benchmark/benchmark_standardized_train.csv')
df_test = pd.read_csv('./data/benchmark/benchmark_standardized_test.csv')

'''
Set features and target columns
'''
df_train_class = pd.DataFrame(df_train['y'])
df_train_features = df_train.loc[:, df_train.columns != 'y']

df_test_class = pd.DataFrame(df_test['y'])
df_test_features = df_test.loc[:, df_test.columns != 'y']

'''
Balanace training subsets using StratifiedKFold
'''
skf_model = StratifiedKFold(n_splits=5,shuffle=True)

'''
Grid search parameters
'''
n_estimators_list = range(10, 50, 10)

'''
Iteration stop
'''
iterations = 5

'''
Automatic hyperparameter tuning given the range and iterations
'''
for i in range(0, iterations):
    print ("---Iteration: ", i)
    avg_acc = np.zeros(shape=[len(n_estimators_list)])
    std_acc = np.zeros(shape=[len(n_estimators_list)])

    x = 0
    for k in n_estimators_list:
        temp_accuracy_list = []

        for train_subset_index, cv_index in skf_model.split(df_train_features,df_train_class):
            df_train_features_subset = df_train_features.loc[train_subset_index]
            df_train_class_subset = df_train_class.loc[train_subset_index]
            df_train_features_cv = df_train_features.loc[cv_index]
            df_train_class_cv = df_train_class.loc[cv_index]

            rf_model = RandomForestClassifier(n_estimators=k, class_weight='balanced')
            rf_model.fit(df_train_features_subset, df_train_class_subset.values.ravel())
            score_value = rf_model.score(df_train_features_cv, df_train_class_cv)
            temp_accuracy_list.append(score_value)

        avg_acc[x] = np.mean(temp_accuracy_list)
        std_acc[x] = np.std(temp_accuracy_list)
        x += 1

    if i==0:
        final_avg_acc = avg_acc
        final_std_acc = std_acc
    else:
        final_avg_acc = np.vstack([final_avg_acc, avg_acc])
        final_std_acc = np.vstack([final_std_acc, std_acc])

final_accuracy_mean_list = np.mean(final_avg_acc, axis=0)
final_k_index = np.argmax(final_accuracy_mean_list)

chosen_k= n_estimators_list[final_k_index]
print ("Cross Validated Estimators for Random Forest Classifier: ",chosen_k)

'''
Balance the data fit for unbalanced dataset
'''
rf_model_final = RandomForestClassifier(n_estimators=chosen_k, class_weight='balanced')
rf_model_final.fit(df_train_features, df_train_class.values.ravel())

predicted_train = rf_model_final.predict(df_train_features)
predicted_test = rf_model_final.predict(df_test_features)

predicted_prob_train = rf_model_final.predict_proba(df_train_features)
predicted_prob_test = rf_model_final.predict_proba(df_test_features)

# filename = ('./model/rf/benchmark_rf_standardized_model.sav')
# pickle.dump(rf_model_final, open(filename, 'wb'))

with open('./models/rf/benchmark_rf_standardized_model.sav', 'wb') as f:
    pickle.dump(rf_model_final, f)

evaluate(df_train_class, predicted_train, predicted_prob_train, df_test_class, predicted_test, predicted_prob_test, 'y')
