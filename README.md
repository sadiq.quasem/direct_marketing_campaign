# Direct Marketing Campaign Analysis

This is a project to analyze direct marketing campaigns (phone calls) of a Portuguese banking
institution. The goal is to analyze this data to draw insights, and then use this data to predict whether or not a client will subscribe a bank term deposit.

The project has the following steps:

1. Data Cleaning
2. Data Preprocessing
3. Model Building
4. Evaluation

The main issues with the dataset are as follows:
1. Null value imputation and dropping
2. Preprocessing required to combine numerical and categorical values
3. Highly imbalanced dataset needed to be accounted for modeling

The following algorithms were built for training and testing on the dataset:

1. Naive Bayes Classifier
2. Support Vector Machines Classifier
3. Random Forest Classifier

Trained models can be found in pickle format in the models folder.
